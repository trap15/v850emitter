OBJECTS = emitcore.o emit.o emittest.o
OUTPUT = emittest
CFLAGS = -m32 -Wall -g
LDFLAGS = -m32

all: $(OUTPUT)
%.o: %.c
	gcc $(CFLAGS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	gcc $(LDFLAGS) -o $(OUTPUT) $(OBJECTS)
clean:
	rm -f $(OUTPUT) $(OBJECTS)
