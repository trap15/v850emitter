/*
	v850emitter - An emitter for V850.

Copyright (C) 2011              Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdarg.h>
#include "emit.h"
#include "emitcore.h"

#define EMIT_BYTE(chunk, byte)				chunk += emit_byte((u8*)chunk, byte)
#define EMIT_HWORD(chunk, hword)			chunk += emit_hword((u8*)chunk, hword)
#define EMIT_WORD(chunk, word)				chunk += emit_word((u8*)chunk, word)

static int emit_byte(u8* chunk, u8 byte)
{
	u8* ref = chunk;
	*chunk++ = byte;
	return chunk - ref;
}

static int emit_hword(u8* chunk, u16 hword)
{
	u8* ref = chunk;
	EMIT_BYTE(chunk, (hword >> 0) & 0xFF);
	EMIT_BYTE(chunk, (hword >> 8) & 0xFF);
	return chunk - ref;
}

static int emit_word(u8* chunk, u32 word)
{
	u8* ref = chunk;
	EMIT_HWORD(chunk, (word >> 0) & 0xFFFF);
	EMIT_HWORD(chunk, (word >> 16) & 0xFFFF);
	return chunk - ref;
}

int v850emit_nop(u8* chunk)
{
	u8* ref = chunk;
	EMIT_HWORD(chunk, 0x0000);
	return chunk - ref;
}

int v850emit_add(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, (a1t == V850ARG_REG) || (a1t == V850ARG_IMM5));
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);

	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	if(a1t == V850ARG_REG) {
		_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
		EMIT_HWORD(chunk, (a2 << 11) | (0xE << 5) | (a1 << 0));
	}else if(a1t == V850ARG_IMM5) {
		a1 &= 0x1F;
		EMIT_HWORD(chunk, (a2 << 11) | (0x12 << 5) | (a1 << 0));
	}
	return chunk - ref;
}

int v850emit_addi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_IMM16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);

	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x30 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_and(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);

	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0xA << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_andi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_IMM16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);

	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x36 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

static int v850emit_bcc(u8* chunk, int a1t, int a1, int code)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP9);
	
	a1 &= 0x1FF;
	a1 >>= 1;
	EMIT_HWORD(chunk, ((a1 & 7) << 4) | ((a1 & 0xF8) << 8) | (0xB << 7) | code);
	return chunk - ref;
}

int v850emit_bgt(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0xF);
}

int v850emit_bge(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0xE);
}

int v850emit_blt(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x6);
}

int v850emit_ble(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x7);
}

int v850emit_bh(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0xB);
}

int v850emit_bnl(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x9);
}

int v850emit_bl(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x1);
}

int v850emit_bnh(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x3);
}

int v850emit_be(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x2);
}

int v850emit_bne(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0xA);
}

int v850emit_bv(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x0);
}

int v850emit_bnv(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x8);
}

int v850emit_bn(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x4);
}

int v850emit_bp(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0xC);
}

int v850emit_bc(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x1);
}

int v850emit_bnc(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x9);
}

int v850emit_bz(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x2);
}

int v850emit_bnz(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0xA);
}

int v850emit_br(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0x5);
}

int v850emit_bsa(u8* chunk, int a1t, int a1)
{
	return v850emit_bcc(chunk, a1t, a1, 0xD);
}

int v850emit_clr1(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_BIT3);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a2 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x7));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x43E << 5) | (a3 << 0));
	EMIT_HWORD(chunk, a2);
	return chunk - ref;
}

int v850emit_cmp(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, (a1t == V850ARG_REG) || (a1t == V850ARG_IMM5));
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	if(a1t == V850ARG_REG) {
		_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
		EMIT_HWORD(chunk, (a2 << 11) | (0xF << 5) | (a1 << 0));
	}else if(a1t == V850ARG_IMM5) {
		a1 &= 0x1F;
		EMIT_HWORD(chunk, (a2 << 11) | (0x13 << 5) | (a1 << 0));
	}
	return chunk - ref;
}

int v850emit_di(u8* chunk)
{
	u8* ref = chunk;
	EMIT_HWORD(chunk, 0x3F << 5);
	EMIT_HWORD(chunk, 0xB << 5);
	return chunk - ref;
}

int v850emit_divh(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x2 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_ei(u8* chunk)
{
	u8* ref = chunk;
	EMIT_HWORD(chunk, 0x43F << 5);
	EMIT_HWORD(chunk, 0xB << 5);
	return chunk - ref;
}

int v850emit_halt(u8* chunk)
{
	u8* ref = chunk;
	EMIT_HWORD(chunk, 0x3F << 5);
	EMIT_HWORD(chunk, 0x9 << 5);
	return chunk - ref;
}

int v850emit_jarl(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP22);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & 1));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	a1 &= 0x3FFFFE;
	EMIT_HWORD(chunk, (a2 << 11) | (0x3C << 5) | (a1 >> 16));
	EMIT_HWORD(chunk, a1 & 0xFFFF);
	return chunk - ref;
}

int v850emit_jmp(u8* chunk, int a1t, int a1)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	EMIT_HWORD(chunk, (0x3 << 5) | a1);
	return chunk - ref;
}

int v850emit_jr(u8* chunk, int a1t, int a1)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP22);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & 1));
	a1 &= 0x3FFFFE;
	EMIT_HWORD(chunk, (0x3C << 5) | (a1 >> 16));
	EMIT_HWORD(chunk, a1 & 0xFFFF);
	return chunk - ref;
}

int v850emit_ld_b(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x38 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_ld_h(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & 1));
	a1 &= 0xFFFE;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x39 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_ld_w(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & 1));
	a1 &= 0xFFFE;
	a1 |= 1;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x39 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_ldsr(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_SYSREG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x3F << 5) | (a1 << 0));
	EMIT_HWORD(chunk, 1 << 5);
	return chunk - ref;
}

int v850emit_mov(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, (a1t == V850ARG_REG) || (a1t == V850ARG_IMM5));
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	if(a1t == V850ARG_REG) {
		_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
		EMIT_HWORD(chunk, (a2 << 11) | (0x0 << 5) | (a1 << 0));
	}else if(a1t == V850ARG_IMM5) {
		a1 &= 0x1F;
		EMIT_HWORD(chunk, (a2 << 11) | (0x10 << 5) | (a1 << 0));
	}
	return chunk - ref;
}

int v850emit_movea(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_IMM16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x31 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_movhi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_IMM16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x32 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_mulh(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, (a1t == V850ARG_REG) || (a1t == V850ARG_IMM5));
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	if(a1t == V850ARG_REG) {
		_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
		EMIT_HWORD(chunk, (a2 << 11) | (0x7 << 5) | (a1 << 0));
	}else if(a1t == V850ARG_IMM5) {
		a1 &= 0x1F;
		EMIT_HWORD(chunk, (a2 << 11) | (0x17 << 5) | (a1 << 0));
	}
	return chunk - ref;
}

int v850emit_mulhi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_IMM16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x37 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_not(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x1 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_not1(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_BIT3);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a2 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x7));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x23E << 5) | (a3 << 0));
	EMIT_HWORD(chunk, a2);
	return chunk - ref;
}

int v850emit_or(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x8 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_ori(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_IMM16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x34 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_reti(u8* chunk)
{
	u8* ref = chunk;
	EMIT_HWORD(chunk, 0x3F << 5);
	EMIT_HWORD(chunk, 0xA << 5);
	return chunk - ref;
}

int v850emit_sar(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, (a1t == V850ARG_REG) || (a1t == V850ARG_IMM5));
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	if(a1t == V850ARG_REG) {
		_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
		EMIT_HWORD(chunk, (a2 << 11) | (0x3F << 5) | (a1 << 0));
		EMIT_HWORD(chunk, (0x5 << 5));
	}else if(a1t == V850ARG_IMM5) {
		a1 &= 0x1F;
		EMIT_HWORD(chunk, (a2 << 11) | (0x15 << 5) | (a1 << 0));
	}
	return chunk - ref;
}

int v850emit_satadd(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, (a1t == V850ARG_REG) || (a1t == V850ARG_IMM5));
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	if(a1t == V850ARG_REG) {
		_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
		EMIT_HWORD(chunk, (a2 << 11) | (0x6 << 5) | (a1 << 0));
	}else if(a1t == V850ARG_IMM5) {
		a1 &= 0x1F;
		EMIT_HWORD(chunk, (a2 << 11) | (0x11 << 5) | (a1 << 0));
	}
	return chunk - ref;
}

int v850emit_satsub(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x5 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_satsubi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_IMM16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x33 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

int v850emit_satsubr(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x4 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_setf(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_COND4);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0xF));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x3F << 5) | (a1 << 0));
	EMIT_HWORD(chunk, (0 << 5));
	return chunk - ref;
}

int v850emit_set1(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_BIT3);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a2 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x7));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x3E << 5) | (a3 << 0));
	EMIT_HWORD(chunk, a2);
	return chunk - ref;
}

int v850emit_shl(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, (a1t == V850ARG_REG) || (a1t == V850ARG_IMM5));
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	if(a1t == V850ARG_REG) {
		_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
		EMIT_HWORD(chunk, (a2 << 11) | (0x3F << 5) | (a1 << 0));
		EMIT_HWORD(chunk, (0x6 << 5));
	}else if(a1t == V850ARG_IMM5) {
		a1 &= 0x1F;
		EMIT_HWORD(chunk, (a2 << 11) | (0x16 << 5) | (a1 << 0));
	}
	return chunk - ref;
}

int v850emit_shr(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, (a1t == V850ARG_REG) || (a1t == V850ARG_IMM5));
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	if(a1t == V850ARG_REG) {
		_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
		EMIT_HWORD(chunk, (a2 << 11) | (0x3F << 5) | (a1 << 0));
		EMIT_HWORD(chunk, (0x4 << 5));
	}else if(a1t == V850ARG_IMM5) {
		a1 &= 0x1F;
		EMIT_HWORD(chunk, (a2 << 11) | (0x14 << 5) | (a1 << 0));
	}
	return chunk - ref;
}

int v850emit_sld_b(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP7);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x7F));
	_V850ASSERT(V850ERROR_BADARG, a2 == V850REG_EP);
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x18 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_sld_h(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP8);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, a2 == V850REG_EP);
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0xFE));
	a1 >>= 1;
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x20 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_sld_w(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_DISP8);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, a2 == V850REG_EP);
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1FC));
	a1 >>= 1;
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x28 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_sst_b(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP7);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x7F));
	_V850ASSERT(V850ERROR_BADARG, a3 == V850REG_EP);
	EMIT_HWORD(chunk, (a1 << 11) | (0x1C << 5) | (a2 << 0));
	return chunk - ref;
}

int v850emit_sst_h(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP8);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0xFE));
	a2 >>= 1;
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x24 << 5) | (a2 << 0));
	return chunk - ref;
}

int v850emit_sst_w(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP8);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1FC));
	a2 >>= 1;
	a2 |= 1;
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x28 << 5) | (a2 << 0));
	return chunk - ref;
}

int v850emit_st_b(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	a2 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x3A << 5) | (a3 << 0));
	EMIT_HWORD(chunk, a2);
	return chunk - ref;
}

int v850emit_st_h(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & 1));
	a2 &= 0xFFFE;
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x3B << 5) | (a3 << 0));
	EMIT_HWORD(chunk, a2);
	return chunk - ref;
}

int v850emit_st_w(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & 1));
	a2 &= 0xFFFE;
	a2 |= 1;
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x3B << 5) | (a3 << 0));
	EMIT_HWORD(chunk, a2);
	return chunk - ref;
}

int v850emit_stsr(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_SYSREG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x3F << 5) | (a1 << 0));
	EMIT_HWORD(chunk, 2 << 5);
	return chunk - ref;
}

int v850emit_sub(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0xD << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_subr(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0xC << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_trap(u8* chunk, int a1t, int a1)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_VECTOR);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	EMIT_HWORD(chunk, (0x3F << 5) | a1);
	EMIT_HWORD(chunk, 0x8 << 5);
	return chunk - ref;
}

int v850emit_tst(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0xB << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_tst1(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_BIT3);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_DISP16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a2 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x7));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a1 << 11) | (0x63E << 5) | (a3 << 0));
	EMIT_HWORD(chunk, a2);
	return chunk - ref;
}

int v850emit_xor(u8* chunk, int a1t, int a1, int a2t, int a2)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	
	_V850ASSERT(V850ERROR_BADARG, !(a1 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	EMIT_HWORD(chunk, (a2 << 11) | (0x9 << 5) | (a1 << 0));
	return chunk - ref;
}

int v850emit_xori(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3)
{
	u8* ref = chunk;
	_V850ASSERT(V850ERROR_UNSUPPORTED, a1t == V850ARG_IMM16);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a2t == V850ARG_REG);
	_V850ASSERT(V850ERROR_UNSUPPORTED, a3t == V850ARG_REG);
	
	a1 &= 0xFFFF;
	_V850ASSERT(V850ERROR_BADARG, !(a2 & ~0x1F));
	_V850ASSERT(V850ERROR_BADARG, !(a3 & ~0x1F));
	EMIT_HWORD(chunk, (a3 << 11) | (0x35 << 5) | (a2 << 0));
	EMIT_HWORD(chunk, a1);
	return chunk - ref;
}

