/*
	v850emitter - An emitter for V850.

Copyright (C) 2011              Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef _EMIT_H
#define _EMIT_H

#include "emitcore.h"

#define V850EMIT_NOP(chunk)					chunk += v850emit_nop((u8*)chunk)
#define V850EMIT_ADD(chunk, a1t, a1, a2t, a2)			chunk += v850emit_add((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_ADDI(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_addi((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_AND(chunk, a1t, a1, a2t, a2)			chunk += v850emit_and((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_ANDI(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_andi((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_BGT(chunk, a1t, a1)				chunk += v850emit_bgt((u8*)chunk, a1t, a1)
#define V850EMIT_BGE(chunk, a1t, a1)				chunk += v850emit_bge((u8*)chunk, a1t, a1)
#define V850EMIT_BLT(chunk, a1t, a1)				chunk += v850emit_blt((u8*)chunk, a1t, a1)
#define V850EMIT_BLE(chunk, a1t, a1)				chunk += v850emit_ble((u8*)chunk, a1t, a1)
#define V850EMIT_BH(chunk, a1t, a1)				chunk += v850emit_bh((u8*)chunk, a1t, a1)
#define V850EMIT_BNL(chunk, a1t, a1)				chunk += v850emit_bnl((u8*)chunk, a1t, a1)
#define V850EMIT_BL(chunk, a1t, a1)				chunk += v850emit_bl((u8*)chunk, a1t, a1)
#define V850EMIT_BNH(chunk, a1t, a1)				chunk += v850emit_bnh((u8*)chunk, a1t, a1)
#define V850EMIT_BE(chunk, a1t, a1)				chunk += v850emit_be((u8*)chunk, a1t, a1)
#define V850EMIT_BNE(chunk, a1t, a1)				chunk += v850emit_bne((u8*)chunk, a1t, a1)
#define V850EMIT_BV(chunk, a1t, a1)				chunk += v850emit_bv((u8*)chunk, a1t, a1)
#define V850EMIT_BNV(chunk, a1t, a1)				chunk += v850emit_bnv((u8*)chunk, a1t, a1)
#define V850EMIT_BN(chunk, a1t, a1)				chunk += v850emit_bn((u8*)chunk, a1t, a1)
#define V850EMIT_BP(chunk, a1t, a1)				chunk += v850emit_bp((u8*)chunk, a1t, a1)
#define V850EMIT_BC(chunk, a1t, a1)				chunk += v850emit_bc((u8*)chunk, a1t, a1)
#define V850EMIT_BNC(chunk, a1t, a1)				chunk += v850emit_bnc((u8*)chunk, a1t, a1)
#define V850EMIT_BZ(chunk, a1t, a1)				chunk += v850emit_bz((u8*)chunk, a1t, a1)
#define V850EMIT_BNZ(chunk, a1t, a1)				chunk += v850emit_bnz((u8*)chunk, a1t, a1)
#define V850EMIT_BR(chunk, a1t, a1)				chunk += v850emit_br((u8*)chunk, a1t, a1)
#define V850EMIT_BSA(chunk, a1t, a1)				chunk += v850emit_bsa((u8*)chunk, a1t, a1)
#define V850EMIT_CLR1(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_clr1((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_CMP(chunk, a1t, a1, a2t, a2)			chunk += v850emit_cmp((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_DI(chunk)					chunk += v850emit_di((u8*)chunk)
#define V850EMIT_DIVH(chunk, a1t, a1, a2t, a2)			chunk += v850emit_divh((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_EI(chunk)					chunk += v850emit_ei((u8*)chunk)
#define V850EMIT_HALT(chunk)					chunk += v850emit_halt((u8*)chunk)
#define V850EMIT_JARL(chunk, a1t, a1, a2t, a2)			chunk += v850emit_jarl((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_JMP(chunk, a1t, a1)				chunk += v850emit_jmp((u8*)chunk, a1t, a1)
#define V850EMIT_JR(chunk, a1t, a1)				chunk += v850emit_jr((u8*)chunk, a1t, a1)
#define V850EMIT_LD_B(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_ld_b((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_LD_H(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_ld_h((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_LD_W(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_ld_w((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_LDSR(chunk, a1t, a1, a2t, a2)			chunk += v850emit_ldsr((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_MOV(chunk, a1t, a1, a2t, a2)			chunk += v850emit_mov((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_MOVEA(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_movea((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_MOVHI(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_movhi((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_MULH(chunk, a1t, a1, a2t, a2)			chunk += v850emit_mulh((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_MULHI(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_mulhi((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_NOT(chunk, a1t, a1, a2t, a2)			chunk += v850emit_not((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_NOT1(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_not1((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_OR(chunk, a1t, a1, a2t, a2)			chunk += v850emit_or((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_ORI(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_ori((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_RETI(chunk)					chunk += v850emit_reti((u8*)chunk)
#define V850EMIT_SAR(chunk, a1t, a1, a2t, a2)			chunk += v850emit_sar((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SATADD(chunk, a1t, a1, a2t, a2)		chunk += v850emit_satadd((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SATSUB(chunk, a1t, a1, a2t, a2)		chunk += v850emit_satsub((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SATSUBI(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_satsubi((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_SATSUBR(chunk, a1t, a1, a2t, a2)		chunk += v850emit_satsubr((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SETF(chunk, a1t, a1, a2t, a2)			chunk += v850emit_setf((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SET1(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_set1((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_SHL(chunk, a1t, a1, a2t, a2)			chunk += v850emit_shl((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SHR(chunk, a1t, a1, a2t, a2)			chunk += v850emit_shr((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SLD_B(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_sld_b((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_SLD_H(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_sld_h((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_SLD_W(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_sld_w((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_SST_B(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_sst_b((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_SST_H(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_sst_h((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_SST_W(chunk, a1t, a1, a2t, a2, a3t, a3)	chunk += v850emit_sst_w((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_ST_B(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_st_b((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_ST_H(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_st_h((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_ST_W(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_st_w((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_STSR(chunk, a1t, a1, a2t, a2)			chunk += v850emit_stsr((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SUB(chunk, a1t, a1, a2t, a2)			chunk += v850emit_sub((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_SUBR(chunk, a1t, a1, a2t, a2)			chunk += v850emit_subr((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_TRAP(chunk, a1t, a1)				chunk += v850emit_trap((u8*)chunk, a1t, a1)
#define V850EMIT_TST(chunk, a1t, a1, a2t, a2)			chunk += v850emit_tst((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_TST1(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_tst1((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)
#define V850EMIT_XOR(chunk, a1t, a1, a2t, a2)			chunk += v850emit_xor((u8*)chunk, a1t, a1, a2t, a2)
#define V850EMIT_XORI(chunk, a1t, a1, a2t, a2, a3t, a3)		chunk += v850emit_xori((u8*)chunk, a1t, a1, a2t, a2, a3t, a3)

int v850emit_nop(u8* chunk);
int v850emit_add(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_addi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_and(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_andi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_bgt(u8* chunk, int a1t, int a1);
int v850emit_bge(u8* chunk, int a1t, int a1);
int v850emit_blt(u8* chunk, int a1t, int a1);
int v850emit_ble(u8* chunk, int a1t, int a1);
int v850emit_bh(u8* chunk, int a1t, int a1);
int v850emit_bnl(u8* chunk, int a1t, int a1);
int v850emit_bl(u8* chunk, int a1t, int a1);
int v850emit_bnh(u8* chunk, int a1t, int a1);
int v850emit_be(u8* chunk, int a1t, int a1);
int v850emit_bne(u8* chunk, int a1t, int a1);
int v850emit_bv(u8* chunk, int a1t, int a1);
int v850emit_bnv(u8* chunk, int a1t, int a1);
int v850emit_bn(u8* chunk, int a1t, int a1);
int v850emit_bp(u8* chunk, int a1t, int a1);
int v850emit_bc(u8* chunk, int a1t, int a1);
int v850emit_bnc(u8* chunk, int a1t, int a1);
int v850emit_bz(u8* chunk, int a1t, int a1);
int v850emit_bnz(u8* chunk, int a1t, int a1);
int v850emit_br(u8* chunk, int a1t, int a1);
int v850emit_bsa(u8* chunk, int a1t, int a1);
int v850emit_clr1(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_cmp(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_di(u8* chunk);
int v850emit_divh(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_ei(u8* chunk);
int v850emit_halt(u8* chunk);
int v850emit_jarl(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_jmp(u8* chunk, int a1t, int a1);
int v850emit_jr(u8* chunk, int a1t, int a1);
int v850emit_ld_b(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_ld_h(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_ld_w(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_ldsr(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_mov(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_movea(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_movhi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_mulh(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_mulhi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_not(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_not1(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_or(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_ori(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_reti(u8* chunk);
int v850emit_sar(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_satadd(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_satsub(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_satsubi(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_satsubr(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_setf(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_set1(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_shl(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_shr(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_sld_b(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_sld_h(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_sld_w(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_sst_b(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_sst_h(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_sst_w(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_st_b(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_st_h(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_st_w(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_stsr(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_sub(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_subr(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_trap(u8* chunk, int a1t, int a1);
int v850emit_tst(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_tst1(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);
int v850emit_xor(u8* chunk, int a1t, int a1, int a2t, int a2);
int v850emit_xori(u8* chunk, int a1t, int a1, int a2t, int a2, int a3t, int a3);

#endif
