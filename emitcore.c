/*
	v850emitter - An emitter for V850.

Copyright (C) 2011              Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdarg.h>
#include "emitcore.h"
#include "emit.h"

int v850emit_prologue(u8* chunk, int cnt, ...)
{
	u8* ref = chunk;
	va_list v;
	va_start(v, cnt);
	return chunk - ref;
}

int v850emit_epilogue(u8* chunk, int cnt, ...)
{
	u8* ref = chunk;
	va_list v;
	va_start(v, cnt);
	return chunk - ref;
}
