/*
	v850emitter - An emitter for V850.

Copyright (C) 2011              Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef _EMITCORE_H
#define _EMITCORE_H

#include <stdio.h>
#include <stdint.h>

typedef uint8_t		u8;
typedef uint16_t	u16;
typedef uint32_t	u32;
typedef uint64_t	u64;
typedef int8_t		s8;
typedef int16_t		s16;
typedef int32_t		s32;
typedef int64_t		s64;

/* Register names. */
typedef enum {
	V850REG_R0	= 0,
	V850REG_R1	= 1,
	V850REG_R2	= 2,
	V850REG_R3	= 3,
	V850REG_R4	= 4,
	V850REG_R5	= 5,
	V850REG_R6	= 6,
	V850REG_R7	= 7,
	V850REG_R8	= 8,
	V850REG_R9	= 9,
	V850REG_R10	= 10,
	V850REG_R11	= 11,
	V850REG_R12	= 12,
	V850REG_R13	= 13,
	V850REG_R14	= 14,
	V850REG_R15	= 15,
	V850REG_R16	= 16,
	V850REG_R17	= 17,
	V850REG_R18	= 18,
	V850REG_R19	= 19,
	V850REG_R20	= 20,
	V850REG_R21	= 21,
	V850REG_R22	= 22,
	V850REG_R23	= 23,
	V850REG_R24	= 24,
	V850REG_R25	= 25,
	V850REG_R26	= 26,
	V850REG_R27	= 27,
	V850REG_R28	= 28,
	V850REG_R29	= 29,
	V850REG_R30	= 30,
	V850REG_R31	= 31,
	
	V850REG_ZERO	= V850REG_R0,
	V850REG_SP	= V850REG_R3,
	V850REG_GP	= V850REG_R4,
	V850REG_TP	= V850REG_R5,
	V850REG_EP	= V850REG_R30,
	V850REG_LP	= V850REG_R31,
} v850regs;

typedef enum {
	V850SYSREG_EIPC		= 0,
	V850SYSREG_EIPSW	= 1,
	V850SYSREG_FEPC		= 2,
	V850SYSREG_FEPSW	= 3,
	V850SYSREG_ECR		= 4,
	V850SYSREG_PSW		= 5,
} v850sysregs;

typedef enum {
	V850COND_V	= 0x0,
	V850COND_NV	= 0x8,
	V850COND_C	= 0x1,
	V850COND_NC	= 0x9,
	V850COND_L	= V850COND_C,
	V850COND_NL	= V850COND_NC,
	V850COND_Z	= 0x2,
	V850COND_NZ	= 0xA,
	V850COND_NH	= 0x3,
	V850COND_H	= 0xB,
	V850COND_S	= 0x4,
	V850COND_NS	= 0xC,
	V850COND_N	= V850COND_S,
	V850COND_P	= V850COND_NS,
	V850COND_T	= 0x5,
	V850COND_SA	= 0xD,
	V850COND_LT	= 0x6,
	V850COND_GE	= 0xE,
	V850COND_LE	= 0x7,
	V850COND_GT	= 0xF,
} v850conds;

typedef enum {
	V850ARG_REG = 0,
	V850ARG_IMM5,
	V850ARG_IMM16,
	V850ARG_DISP9,
	V850ARG_BIT3,
	V850ARG_DISP16,
	V850ARG_DISP22,
	V850ARG_SYSREG,
	V850ARG_COND4,
	V850ARG_DISP7,
	V850ARG_DISP8,
	V850ARG_VECTOR,
} v850argtypes;

typedef enum {
	V850ERROR_UNKNOWN = 0,
	V850ERROR_UNSUPPORTED,
	V850ERROR_BADARG,
} v850errors;

#define _V850ASSERT(err, cond)	do { if(!(cond)) { _v850emit_error(err); return 0; } } while(0);

#define V850EMIT_PROLOGUE(cnt...)	chunk += v850emit_prologue((u8*)chunk, cnt)
#define V850EMIT_EPILOGUE(cnt...)	chunk += v850emit_epilogue((u8*)chunk, cnt)

static const char* v850errinfo[] = {
	"Unknown Error",
	"Unsupported emission",
	"Bad argument",
};

#define _v850emit_error(err)	fprintf(stderr, "Error %d on line %d in %s: %s\n", err, __LINE__, __FILE__, v850errinfo[err])

int v850emit_prologue(u8* chunk, int cnt, ...);
int v850emit_epilogue(u8* chunk, int cnt, ...);

#endif
